package sql;

class Mahasiswa {
    int nim;
    String nama;
    Kelamin kelamin;
    String jurusan;
    String alamat;
    
    Mahasiswa(int nim, String nama, Kelamin kelamin, String jurusan, String alamat) {
        this.nim = nim;
        this.nama = nama;
        this.kelamin = kelamin;
        this.jurusan = jurusan;
        this.alamat = alamat;
    }
    
    static public enum Kelamin {
        L,
        P
    }
    
    public void print() {
        System.out.printf("Nim: %d\n" + 
                "Nama: %s\n" + 
                "Kelamin: %s\n" + 
                "Jurusan: %s\n" + 
                "Alamat: %s\n", 
                nim, 
                nama, 
                kelamin.name(), 
                jurusan, 
                alamat
        );
    }
    
    public Object[] toArray() {
        return new Object[]{nama, nim, (kelamin == Kelamin.L) ? "Laki-laki" : "Perempuan", jurusan, alamat};
    }
    
}

