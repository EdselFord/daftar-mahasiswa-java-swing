package sql;

import java.sql.*;
import java.util.ArrayList;

public class MahasiswaModel {
    Connection connection;
    
    MahasiswaModel() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/akademik_java",
                    "root",
                    ""
            );
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public ArrayList<Mahasiswa> getMahasiswa() {
        ArrayList<Mahasiswa> mhs = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();

            String query = "SELECT * FROM mahasiswa";
            ResultSet result = statement.executeQuery(query);
            
            
            while (result.next()) {
                int nim = result.getInt("nim");
                String nama = result.getString("nama");
                Mahasiswa.Kelamin kelamin = Mahasiswa.Kelamin.valueOf(
                        result.getString("kelamin"));
                String jurusan = result.getString("jurusan");
                String alamat = result.getString("alamat");
                
                mhs.add(new Mahasiswa(nim, nama, kelamin, jurusan, alamat));
            }
            
            result.close();
            statement.close();
            
        } catch(Exception e) {
            e.printStackTrace();
        }
        return mhs;
    }
    
    
    public boolean createMahasiswa(String nama, String nim, Mahasiswa.Kelamin kelamin, String jurusan, String alamat) {
        try {
            Statement statement = connection.createStatement();
            
            String query = String.format(
                    "INSERT INTO mahasiswa VALUES ('%s', '%s', '%s', '%s', '%s')",
                    nim, nama, kelamin.name(), jurusan, alamat
                    );
            
            statement.execute(query);
            statement.close();
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean deleteMahasiswa(int nim) {
        try {
            Statement statement = connection.createStatement();
            
            String query = String.format("DELETE FROM mahasiswa WHERE nim='%d'", nim);
            
            statement.execute(query);
            statement.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean updateMahasiswa(int nim, String nama, Mahasiswa.Kelamin kelamin, String jurusan, String alamat) {
        try {
            Statement statement = connection.createStatement();
            
            String query = String.format(
                    "UPDATE mahasiswa SET nama = '%s', kelamin = '%s', jurusan = '%s', alamat = '%s' WHERE nim = '%d'",
                    nama, kelamin.name(), jurusan, alamat, nim
                    );
            
            statement.execute(query);
            statement.close();
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}